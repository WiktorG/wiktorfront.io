const path = require('path')

module.exports = {
  siteMetadata: {
    title: `Wiktor Gała - Front-End Developer`,
    description: `To ja! Twój przyszły Front-End Developer! Tworzenie nowoczesnych stron i aplikacji internetowych to moja pasja! Wejdź i dowiedz się więcej.`,
    author: `@wiktorgala`,
  },
  plugins: [
    `gatsby-plugin-sass`,
    `gatsby-image`,
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-json`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/assets/images/favico.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-alias-imports`,
      options: {
        alias: {
          "@template": path.resolve(__dirname, "src/components/template"),
          "@components": path.resolve(__dirname, "src/components"),
          "@assets": path.resolve(__dirname, "src/assets"),
          "@pages": path.resolve(__dirname, "src/pages"),
          "@content": path.resolve(__dirname, "src/content"),
          "@sass": path.resolve(__dirname, "src/sass"),
        },
      }
    },
    {
      resolve: "gatsby-plugin-web-font-loader",
      options: {
        google: {
          families: ['Montserrat:100,200,300,400,500,600,700,800,900&display=swap']
        }
      },
    },
    {
      resolve: 'gatsby-plugin-i18n',
      options: {        
        langKeyDefault: 'pl',
        useLangKeyLayout: false,
        pagesPaths: [ 'src/pages/pl', 'src/pages/en/']
      }
    }
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    // 'gatsby-plugin-offline',
  ],
}
