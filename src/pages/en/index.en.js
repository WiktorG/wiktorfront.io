import React, { useEffect } from "react"
import ReactGA from 'react-ga';

import Layout from "@components/layout"
import SEO from "@components/seo"

import Header from "@template/sections/header";
import AboutMe from "@template/sections/about-me";
import Skills from "@template/sections/skills";
import FloatingShapes from '@template/floating-shapes';
import MileSteps from "@template/sections/mile-steps";
import MoreToDo from "@template/sections/more-to-do";
import Footer from "@template/sections/footer";

import EnglishContent from '@content/en.json';


import '@sass/style.scss'

const IndexPage = () => { 
  function initializeReactGA() {
    ReactGA.initialize('UA-99634444-2');
    ReactGA.pageview('/en');
  }

  useEffect( () => { 
    initializeReactGA();
  }, [] )

  return (
  <Layout>
    <SEO title="Home" keywords={[`wiktor gała`, `programming`, `react`, 'websites', 'making websites', 'html', 'css', 'javascript', 'js', 'frontend developer', 'web developer', 'websites poland wschowa', 'websites wrocław']} description="It's me! Front-End Developer you're looking for! Developing modern websites and apps is my passion! Come here to find out more."  />
    <Header content={EnglishContent.header} />
    <AboutMe content={EnglishContent.aboutMe} />
    <Skills content={EnglishContent.skills} />
    <FloatingShapes />
    <MileSteps content={EnglishContent.road} />
    <MoreToDo content={EnglishContent.moreToDo} />
    <Footer content={EnglishContent.footer} />
  </Layout>
  ) 
}

export default IndexPage
