import React, { useEffect } from "react"
import ReactGA from 'react-ga';

import Layout from "@components/layout"
import SEO from "@components/seo"

import Header from "@template/sections/header";
import AboutMe from "@template/sections/about-me";
import Skills from "@template/sections/skills";
import FloatingShapes from "@template/floating-shapes";
import MileSteps from "@template/sections/mile-steps";
import MoreToDo from "@template/sections/more-to-do";
import Footer from "@template/sections/footer";

import PolishContent from '@content/pl.json';

import '@sass/style.scss'

const IndexPage = () => {

  function initializeReactGA() {
    ReactGA.initialize('UA-99634444-2');
    ReactGA.pageview('/');
  }

  useEffect( () => { 
    initializeReactGA();
  }, [] );

  return (
  <Layout>
    <SEO title="Strona Główna" keywords={[`wiktor gała`, `programowanie`, `react`, 'strony internetowe', 'tworzenie stron internetowych', 'html', 'css', 'javascript', 'js', 'frontend developer', 'web developer', 'strony internetowe wschowa', 'strony internetowe wrocław']} description="To ja! Twój przyszły Front-End Developer! Tworzenie nowoczesnych stron i aplikacji internetowych to moja pasja! Wejdź i dowiedz się więcej." />
    <Header content={PolishContent.header} />
    <AboutMe content={PolishContent.aboutMe} />
    <Skills content={PolishContent.skills} />
    <FloatingShapes />
    <MileSteps content={PolishContent.road} />
    <MoreToDo content={PolishContent.moreToDo} />
    <Footer content={PolishContent.footer} />
  </Layout>
  )
}

export default IndexPage
