import React from 'react'
import { useInView } from 'react-intersection-observer'

import { useSpring, animated } from 'react-spring'

import SkillsWheel from './../skills-wheel';

const Skills = (props) => {

    const [titleRef, titleInView] = useInView({
        triggerOnce: true,
        threshold: 0.3,
    })

    const titleAnimation = useSpring({
        opacity: titleInView ? 1 : 0,
        transform: titleInView ? `translateX(${0}px)` : `translateX(-${100}px)`
    })

    const [paragraphRef, paragraphInView] = useInView({
        triggerOnce: true,
        threshold: 0.1,
    })

    const paragraphAnimation = useSpring({
        opacity: paragraphInView ? 1 : 0,
        transform: paragraphInView ? `translateX(${0}px)` : `translateX(-${100}px)`,
        delay: 100
    })

  return (
    <section className="skills">
        <div className="container">
            <div className="row row--skills">
                <div className="col-7 skills__text-con">
                    <animated.h1 className="section__title skills__title font--900 font--cyan"
                        ref={titleRef}
                        style={titleAnimation}
                    >
                        {props.content.title}
                    </animated.h1>
                    <animated.p
                     ref={paragraphRef}
                     style={paragraphAnimation}
                    >
                        {props.content.text}
                    </animated.p>
                </div>
                <div className="col-5 skills__wheel-placeholder">
                    <SkillsWheel />
                </div>
            </div>
        </div>
    </section>
  )
}

export default Skills

