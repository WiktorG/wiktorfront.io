import React, { useState } from 'react'
import axios from 'axios';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLinkedinIn, faGithub } from '@fortawesome/free-brands-svg-icons'
import { faHandPeace, faBan } from '@fortawesome/free-solid-svg-icons'

import Notification from './../../notification';

const Footer = (props) => {
    const [email, setEmail] = useState('');
    const [name, setName] = useState('');
    const [message, setMessage] = useState('')
    const [notificationMessage, setNotificationMessage] = useState();
    const [notificationVisible, setNotificationVisible] = useState(false);

    const isBlank = (str) => {
        return (!str || /^\s*$/.test(str));
    }

    const handleSubmit = (e) => { 
        e.preventDefault();
        if (isBlank(email) || isBlank(name) || isBlank(message) ) {
            setNotificationMessage((<span>{`${props.content.errorNotification} `}<FontAwesomeIcon icon={faBan} /></span>));
            setNotificationVisible(true);
        } else { 
            setNotificationMessage((<span>{`${props.content.successNotification} `}<FontAwesomeIcon icon={faHandPeace} /></span>));
            setNotificationVisible(true);
            e.currentTarget.reset();
            const mailData = {
                email: email,
                name: name,
                message: message,
            }
            axios.post('mailer.php', mailData)
            setEmail(''); setName(''); setMessage('');
        }
        window.setTimeout(() => {
        setNotificationVisible(false);
        }, 2000)
    }

    return (
        <>
        <Notification message={notificationMessage} visible={notificationVisible} />
        <footer>
            <div className="footer__form-container font--white">
                <h2 className="section__title footer__title font--900">{props.content.title}</h2>
                <form action="" className="footer__form" onSubmit={handleSubmit}>
                    <div className="form__row">
                        <input name="name" id="name" type="text" placeholder={props.content.namePlaceholder} onChange={(e) => setName(e.target.value)}/>
                        <input name="email" id="email" type="email" placeholder={props.content.emailPlaceholder} onChange={(e) => setEmail(e.target.value)}/>
                    </div>
                    <div className="form__row">
                        <textarea name="message" id="message" placeholder={props.content.messagePlaceholder} onChange={(e) => setMessage(e.target.value)}></textarea>
                    </div>
                    <div className="form__row form__row--buttons">
                        <div className="form__buttons">
                            <span>
                                <a href="https://www.linkedin.com/in/wiktor-ga%C5%82a-290909138/" className="font--white"><FontAwesomeIcon icon={faLinkedinIn} /></a>
                                <a href="https://github.com/WiktorG" className="font--white"><FontAwesomeIcon icon={faGithub} /></a>
                            </span>
                            <button type="submit">
                                {props.content.buttonLabel}
                            </button>
                        </div>
                    </div>
                </form>
                <span className="footer__rights">&copy; {` ${props.content.rights}`}</span>
            </div>
        </footer>
        </>
    )
}

export default Footer
