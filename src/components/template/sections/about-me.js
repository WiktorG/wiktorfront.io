import React from 'react'

import { StaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import { useInView } from 'react-intersection-observer'

import AboutIcons from './../about-icons';

import { Keyframes } from 'react-spring/renderprops';
import { useSpring, animated } from 'react-spring';

const AboutMe = (props) => {
  
  const Gradient = Keyframes.Spring(async next => {
      while (true)
        await next({
          from: { 
            transform: 0,
           }, 
          to: { 
            transform: Math.PI,
           }, 
          reset: true, 
          config: {
            friction: 2,
            tension: 1
          }
        })
  });

  const [titleRef, titleInView] = useInView({
    triggerOnce: true,
    threshold: 0.3,
  })

  const titleAnimation = useSpring({
    opacity: titleInView ? 1 : 0,
    transform: titleInView ? `translateX(${0}px)` : `translateX(${100}px)`
  })
  
  const [paragraphRef, paragraphInView] = useInView({
    triggerOnce: true,
    threshold: 0.3,
  })
  const paragraphAnimation = useSpring({
    opacity: paragraphInView ? 1 : 0,
    transform: paragraphInView ? `translateX(${0}px)` : `translateX(${100}px)`,
    delay: 100
  })

  return (
    <section className="about-me" id="about-me">
        <div className="container">
            <div className="row row--about">
              <div className="col-3 about__photo-con">
                <StaticQuery
                  query={graphql`
                    query {
                      placeholderImage: file(relativePath: { eq: "photo.png" }) {
                        childImageSharp {
                          fluid(maxWidth: 1024) {
                            ...GatsbyImageSharpFluid
                          }
                        }
                      }
                    }
                  `}
                  render={data => 
                    <Gradient>
                      {(props) => {
                        return (
                          <span 
                            className="about__photo--gradient-con"
                          >
                            <Img
                              className="about__photo"
                              fluid={data.placeholderImage.childImageSharp.fluid} 
                            />
                            <span 
                            className="about__photo--gradient"
                            style={{
                              background: (() =>  { 
                                return `linear-gradient(135deg, #ff2585 ${Math.sin(props.transform)*40}%, #39d4e5 ${Math.sin(props.transform)*40 + 50}%)` 
                              })(),
                              opacity: 0.3,
                            }} 
                            />
                          </span>
                        )
                      }}
                    </Gradient>
                  }
                />
              </div>
              <div className="col-9 about__text-con">
                    <animated.h1 
                      ref={titleRef}
                      style={titleAnimation}
                      className="section__title about__title font--cyan font--900">
                      {props.content.title}
                    </animated.h1>
                    <animated.p
                      ref={paragraphRef}
                      style={paragraphAnimation}
                    >
                      {props.content.text}
                    </animated.p>
              </div>
            <AboutIcons />
            </div>
        </div>
    </section>
  )
}


export default AboutMe;