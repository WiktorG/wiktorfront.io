import React, { useState, useEffect } from 'react'

import { useInView } from 'react-intersection-observer'

import { useSpring, animated } from 'react-spring';

import shoe from '@assets/images/shoe.png';

const MileSteps = (props) => {

    const stepsContent = [...props.content.steps]

    const [titleRef, titleInView] = useInView({
        triggerOnce: true,
        threshold: 0.3,
    })

    const titleAnimation = useSpring({
        opacity: titleInView ? 1 : 0,
        transform: titleInView ? `translateX(${0}px)` : `translateX(-${100}px)`
    })

    const steps = stepsContent.map((step, index) => {

        const [windowWidth, setWindowWidth] = useState(768)

        useEffect( () => {
            setWindowWidth(window.innerWidth)
            window.addEventListener( 'resize', () => { 
                setWindowWidth(window.innerWidth)
            })
        }, [] );

        const [stepRef, stepInView] = useInView({
            threshold: 0.7,
        });

        const stepDelay = windowWidth < 768 ? 0 : 200;

        const stepAnimation = useSpring({
            opacity: stepInView ? 1 : 0,
            x: stepInView ? 0 : 20, 
            delay: stepInView ? (stepDelay * index) : 0,
        })

        const stepDateAnimation = useSpring({
            opacity: stepInView ? 1 : 0,
            x: stepInView ? 0 : 20, 
            height: stepInView ? 20 : 0,
            delay: stepInView ? (stepDelay * index) + 200 : 50,
        })

        const stepTextAnimation = useSpring({
            opacity: stepInView ? 1 : 0,
            delay: stepInView ? (stepDelay * index) + 300 : 100,
        })

        return (
        <animated.div 
            className="mile__step-set" 
            key={index}
            ref={stepRef}
            style={{ 
                ...stepAnimation,
                transform: stepAnimation.x.interpolate(x => `translate3d(-${x}px,0,0)`)
            }}
        >
            <img src={shoe} className="step__shoe step__shoe--right" alt="ikona buta" />
            <div className="step__content-container">
                <img src={shoe} className="step__shoe step__shoe--left" alt="ikona buta" />
                <span className="step__text-container">
                    <animated.h3 
                        className="step__time font--700"
                        style={{
                            ...stepDateAnimation,
                            transform: stepAnimation.x.interpolate(x => `translate3d(0,-${x}px,0)`)
                        }}
                    >
                        {step.time}
                    </animated.h3>
                    <animated.span 
                        className="step__desc" 
                        dangerouslySetInnerHTML={{ __html: step.text}} 
                        style={{
                            ...stepTextAnimation,
                        }}
                    />
                </span>
            </div>
        </animated.div>
        ) } 
    )

    return (
        <section className="mile-steps">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <animated.h1 
                            className="section__title font--900 font--cyan"
                            ref={titleRef}
                            style={titleAnimation}
                        >
                            {props.content.title}
                        </animated.h1>
                    </div>
                </div>
                <div className="row row--mile-steps">
                    {steps}
                </div>
            </div>
        </section>
    )
}

export default MileSteps