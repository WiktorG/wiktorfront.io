import React from 'react'
import { Link } from 'gatsby'
import { Link as ScrollLink } from "react-scroll"

import Particles from 'react-particles-js';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCode, faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons'

import { useSpring, animated } from 'react-spring'

const Header = (props) =>  {

    const baseAnimationParams = {
      from: {
        opacity: 0.1,
        transform: `translateX(${100}px)`,
      },
      to: {
        opacity: 1,
        transform: `translateX(${0}px)`,
      },
      delay: 100,
    }

    const mainSloganAnimation = useSpring({
      ...baseAnimationParams,
      config: {
        tension: 90,
        friction: 3,
      }
    });

    const subSloganAnimation = useSpring({
      ...baseAnimationParams,
      config: {
        tension: 80,
        friction: 3,
      },
      delay: 300,
    });

    const arrowAnimation = useSpring({
      from: { 
        transform: `translateY(${10}px) translateX(-50%)`,
      },
      to: { 
        transform: `translateY(${0}px) translateX(-50%)`,
      },
      delay: 500,
      config: {
        tension: 1000,
        friction: 3,
        mass: 10,
      },
    })

    const captionAnimation = useSpring({
      ...baseAnimationParams,
      config: {
        tension: 100,
        friction: 3,
      },
      delay: 450,
    });

    const particlesParams = {
        particles: {
          color: {value: ["#39d4e5", "#ff2585"]},
          line_linked: {
            enable: false
          },
          move: {
            speed: 1,
            direction: 'left',
            straight: true,
            out_mode: 'out',
            random: true,
          },
          opacity: {
            value: 1,
            anim: {
              enable: false,
            }
          },
          size: {
            value: 5.1,
            random: true,
            anim: {
              enable: false
            }
          },
          number: {
            value: 40,
            density: {
              enable: true,
              value_area: 800
            }
          },
          shape: {
            type: "circle",
          },
        }
    }
    return (
      <header>
        <Particles 
        className="header__particles" 
        canvasClassName="header__particles-canvas"
        params={particlesParams}
        width="100%"
        height="280px"
        />       
        <Link to={props.content.langUrl} className="font--pink font--800 header__language-switch">{props.content.lang}</Link>
        <div className="container">
          <div className="row">
            <span className="header__slogans">
                <animated.h1 
                className="font--900 font--pink" 
                style={mainSloganAnimation}>
                {props.content.greeting}
                </animated.h1>
                <animated.h2 
                className="font--100 font--pink" 
                style={subSloganAnimation}>
                {props.content.slogan}
                </animated.h2>
                <animated.h3
                className="font--900 font--cyan" 
                style={captionAnimation}>
                {props.content.sloganBold}
                <FontAwesomeIcon icon={faCode} className="slogan__icon"/>
                </animated.h3>
            </span>
            <ScrollLink
            smooth={true}
            duration={300}
            to="about-me"
            className="header__scroll" 
            style={arrowAnimation}
            >
              <FontAwesomeIcon icon={faAngleDoubleDown} className="scroll__arrows"/>
            </ScrollLink>
          </div>
        </div>
      </header>
    )
}

export default Header
