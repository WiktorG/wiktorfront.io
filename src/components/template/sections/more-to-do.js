import React from 'react'
import { useInView } from 'react-intersection-observer';

import { useSpring, animated } from 'react-spring';


const MoreToDo = (props) => {

    
  const [moreToDoRef, moreToDoInView] = useInView({
      threshold: 0.5,
  })

  const titleAnimation = useSpring({
      opacity: moreToDoInView ? 1 : 0,
      transform: moreToDoInView ? `translateY(${0}px)` : `translateY(${50}px)`
  })

  const spanAnimation = useSpring({
      display: 'block',
      opacity: moreToDoInView ? 1 : 0,
      transform: moreToDoInView ? `translateY(${0}px)` : `translateY(${100}px)`,
      delay: 100
  })

    return (
      <section className="more-to-do">
          <div className="container" ref={moreToDoRef}>
              <animated.h2 
                className="section__title more-to-do__title font--cyan font--900"
                style={titleAnimation}
              >
                {props.content.title}
              </animated.h2>
              <animated.span 
                className="more-to-do__subtitle"
                style={spanAnimation}
              >
                {props.content.subtitle}
              </animated.span>
          </div>
      </section>
    )
}

export default MoreToDo
