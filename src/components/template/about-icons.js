/* eslint-disable */
import React from 'react'
import { useInView } from 'react-intersection-observer'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPowerOff, faSuperscript, faRulerHorizontal } from '@fortawesome/free-solid-svg-icons'

import triangles from '../../assets/images/triangles.png';

import { useSpring, animated } from 'react-spring';

const AboutIcons = () => {

  const [refIcons, iconsInView] = useInView({})

  const baseSlideRightAnimation = {
    opacity: iconsInView ? 1 : 0,
    transform: iconsInView ? `translateX(${0})` : `translateX(-${200}px)`,
  }

  const baseSlideLeftAnimation = {
    opacity: iconsInView ? 1 : 0,
    transform: iconsInView ? `translateX(${0})` : `translateX(${200}px)`,
  }

  const powerAnimation = useSpring({
     ...baseSlideRightAnimation,
     delay: 100,
  });

  const xPowerAnimation = useSpring({
    ...baseSlideRightAnimation,
    delay: 200,
  });

  const rulerAnimation = useSpring({
    ...baseSlideLeftAnimation,
    delay: 100,
  })

  const trianglesAnimation = useSpring({
    ...baseSlideLeftAnimation,
    delay: 200,
  })

  return (
    <div className="container about__icons">
      <div className="row" ref={refIcons}>
          <animated.span
            style={powerAnimation}  
            className="a__icon--power"    
          >
            <FontAwesomeIcon 
            icon={faPowerOff} 
            />
          </animated.span>
          <animated.span
            style={xPowerAnimation}
            className="a__icon--superscript" 
          >
            <FontAwesomeIcon 
            icon={faSuperscript} 
            />
          </animated.span>
          <animated.span
            style={rulerAnimation}
            className="a__icon--ruler" 
          >
            <FontAwesomeIcon 
            icon={faRulerHorizontal} 
            />
          </animated.span>
          <animated.span
            style={trianglesAnimation}
            className="a__icon--triangles"           
          >
            <img
              src={triangles}
            />
          </animated.span>
      </div>
    </div>
  )
}

export default AboutIcons;
