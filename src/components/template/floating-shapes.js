import React from 'react'

import { useInView } from 'react-intersection-observer'

import PinkShape from './../../assets/images/pink-shape.png';
import CyanShape from './../../assets/images/cyan-shape.png';

import { useSpring, animated } from 'react-spring';


const FloatingShapes = () => {

    const [ref, inView] = useInView({
        triggerOnce: true
    });

    const pinkStyles = useSpring({ 
        config: { 
            mass: 3,
            tension: 100,
            friction: 12,
        },
        transform: inView ? `translateX(${-25})` :  `translateX(-${100}%)`,
        opacity: inView ? 1 : 0,
    });

    const cyanStyles = useSpring({ 
        config: { 
            mass: 1,
            tension: 100,
            friction: 12,
        },
        transform: inView ? `translateX(${10})` :  `translateX(${100}%)`,
        opacity: inView ? 1 : 0,
    });

    return (
        <div className="container--f floating-shapes" >
            <animated.img 
                src={PinkShape} 
                alt="ornament" 
                className="shape__pink"
                style={pinkStyles} 
                ref={ref}
                />
            <animated.img 
                src={CyanShape} 
                alt="ornament" 
                className="shape__cyan"
                style={cyanStyles}
                />
        </div>
    )
}

export default FloatingShapes
