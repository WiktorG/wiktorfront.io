import React from 'react'
import lifecycle from 'react-pure-lifecycle';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPhp, faSass, faJs, faCss3Alt, faHtml5, faReact, faWordpressSimple, faGit } from '@fortawesome/free-brands-svg-icons'

const SkillsWheelLifecycleMethods = {
  componentDidMount() {
  }
}

const SkillsWheel = () => {

  return (
    <div className="skills__wheel-con">
      <span className="skills__wheel-con-filler"></span>
      <span className="skills__wheel">
        <FontAwesomeIcon icon={faPhp} className="skill skill--php" />
        <FontAwesomeIcon icon={faSass} className="skill skill--sass" />
        <FontAwesomeIcon icon={faJs} className="skill skill--js" />
        <FontAwesomeIcon icon={faCss3Alt} className="skill skill--css" />
        <FontAwesomeIcon icon={faHtml5} className="skill skill--html" />
        <FontAwesomeIcon icon={faReact} className="skill skill--react" />
        <FontAwesomeIcon icon={faWordpressSimple} className="skill skill--wordpress" />
        <FontAwesomeIcon icon={faGit} className="skill skill--git" />
      </span>
    </div>
  )
}

export default lifecycle(SkillsWheelLifecycleMethods)(SkillsWheel);