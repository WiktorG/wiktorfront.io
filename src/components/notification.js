import React from 'react'
import PropTypes from 'prop-types';
import { useSpring, animated } from 'react-spring';

const  Notification = (props) => {

    const toggleAnimation = useSpring({ 
        opacity: props.visible ? 1 : 0,
        transform: props.visible ? `translateY(${0}px) translateX(-50%)` : `translateY(-${50}px) translateX(-50%)`,
    })
    return (
        <animated.div 
            className="notification"
            style={toggleAnimation}
        >
            {props.message}
        </animated.div>
    )
}

Notification.defaultProps = {
    visible: false,
    message: (<></>),
};

Notification.propTypes = {
    visible: PropTypes.bool.isRequired,
    message: PropTypes.object.isRequired,
};

export default Notification
